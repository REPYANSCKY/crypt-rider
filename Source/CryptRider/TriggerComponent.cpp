// Fill out your copyright notice in the Description page of Project Settings.


#include "TriggerComponent.h"

UTriggerComponent::UTriggerComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UTriggerComponent::BeginPlay()
{
	Super::BeginPlay();
}

AActor* UTriggerComponent::GetAcceptableActor() const
{
	TArray<AActor*> Actors;
	GetOverlappingActors(Actors);
	
	for (auto Actor : Actors)
	{
		if (Actor->ActorHasTag(ComponentTag) && !(Actor->ActorHasTag("Grabbed")))
		{
			return Actor;
		}
	}
	
	return nullptr;
}

void UTriggerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	auto Actor = GetAcceptableActor();
	if (Actor != nullptr && DoorIsLocked)
	{
		auto Component = Cast<UPrimitiveComponent>(Actor->GetRootComponent());
		if (Component != nullptr)
		{
			Component->SetSimulatePhysics(false);
		}
		Actor->AttachToComponent(this, FAttachmentTransformRules::KeepWorldTransform);
		Actor->SetActorLocationAndRotation(FinishLocation, FinishRotation);
		Mover->SetShouldMove(true, GetWorld()->TimeSeconds);
		if(isDoor)
		{
			MoverOther->SetShouldMove(true, GetWorld()->TimeSeconds);
		}
		DoorIsLocked = false;
	}
	else if(!DoorIsLocked && Actor == nullptr)
	{
		Mover->SetShouldMove(false, GetWorld()->TimeSeconds);
		if(isDoor)
		{
			MoverOther->SetShouldMove(false, GetWorld()->TimeSeconds);
		}
		DoorIsLocked = true;
	}
}
